/*
	curldate: Echo the current date for cURL, for use as a basic NTP alternative
	
	Server usage:
		curldate [port]
	Client usage:
		date -s $(curl [server]:[port])
	
	Copyright (C) 2022 ikku
	License: BSD-0
	Based on (CC BY-SA 4.0):
	http://rosettacode.org/wiki/Hello_world/Web_server#Rust
	https://gist.github.com/mjohnsullivan/e5182707caf0a9dbdf2d
*/
use std::net::TcpListener;
use std::io::{Read, Write};
use std::process::Command;
use std::env::args;
use std::process::exit;
fn main() {
	let port: &String;
	let argv: Vec<String> = args().collect();
	if argv.len() == 2 {
		match argv[1].parse::<u16>() {
			Ok (_) => port = &argv[1],
			Err(_) => { println!("Invalid port number"); exit(1) }
		}
	} else {
		println!("Usage: curldate [port]");
		if argv.len() != 1 { println!("Error: Wrong number of arguments"); }
		exit(0);
	}
	let listener = TcpListener::bind("127.0.0.1:".to_owned() + port).unwrap();
	println!("Starting on port {}", port);
	for stream in listener.incoming() {
		match stream {
			Ok(mut stream) => {
				let command = Command::new("date").output()
					.expect("Failed to execute 'date'").stdout;
				let response = [
					b"HTTP/1.1 200 OK\nContent-Type: text/plain; charset=UTF-8\n\n",
					command.as_slice()
				].concat();
				match &stream.write(&response) {
					Ok(_) => println!("Response sent"),
					Err(e) => println!("Failed sending response: {}", e),
				}
				// take the response and eat it so the client doesn't complain
				let mut buf = [0u8;4096]; stream.read(&mut buf).unwrap();
			}
			Err(e) => {println!("Unable to connect: {}", e);}
		}
	}
}